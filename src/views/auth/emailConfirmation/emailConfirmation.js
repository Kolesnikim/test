import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

// Chakra imports
import {
  Box,
  Button,
  Flex,
  Heading,
  useColorModeValue,
} from "@chakra-ui/react";
import { useEmailConfirmation } from "hooks/useEmailConfirmation";

// Custom components
import DefaultAuth from "layouts/auth/types/Default";

// Assets
import illustration from "assets/img/auth/auth.png";

function EmailConfirmation() {
  // Chakra color mode
  const textColor = useColorModeValue("navy.700", "white");
  const textColorSecondary = "gray.400";
  const [success, setSuccess] = useState("");
  const { emailConfirmation, isLoading, error } = useEmailConfirmation();
  const history = useHistory();

  const handleConfirmation = async () => {
    let url_string = window.location.href;
    let splitted = url_string.split("confirmation_token=");
    let res = await emailConfirmation(splitted[1]);
    if (res) {
      console.log("LOGIN RES: ", res);
      setSuccess("Your email successfully confirmed!");
    }
  };

  useEffect(() => {
    handleConfirmation();
  }, []);

  return (
    <DefaultAuth illustrationBackground={illustration} image={illustration}>
      <Flex
        w="100%"
        maxW="max-content"
        mx={{ base: "auto", lg: "0px" }}
        me="auto"
        h="100%"
        alignItems="center"
        justifyContent="center"
        px={{ base: "25px", md: "0px" }}
        flexDirection="column"
      >
        <Box me="auto" alignItems={"center"}>
          <Heading color={textColor} fontSize={{ base: "3xl", md: "36px" }}>
            {success} {error}
          </Heading>
        </Box>
        {success && (
          <Button
            onClick={() => {
              history.push("/projects");
            }}
            variant={"brand"}
            minW={"200px"}
            maxW={"200px"}
            fontSize="md"
            size="lg"
            fontWeight="500"
            borderRadius="70px"
            mt="15px"
          >
            Continue
          </Button>
        )}
      </Flex>
    </DefaultAuth>
  );
}

export default EmailConfirmation;
