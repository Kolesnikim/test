// Chakra imports
import {
  Flex,
  FormLabel,
  Input,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
// Custom components
import React from "react";
import DateTimePicker from "react-datetime-picker";

export function TimePicker({ label, action, value }) {
  // Chakra Color Mode
  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
  const brandStars = useColorModeValue("brand.500", "brand.400");

  return (
    <Flex direction="column" mb={"30px"}>
      <FormLabel
        display="flex"
        ms="10px"
        fontSize="sm"
        color={textColorPrimary}
        fontWeight="bold"
        _hover={{ cursor: "pointer" }}
      >
        {label} <Text color={brandStars}>*</Text>
      </FormLabel>
      <DateTimePicker
        onChange={action}
        value={value}
        format={"d/M/y HH:mm"}
        isClockOpen={false}
        disableClock={true}
        // format={"dd/MM/y HH:mm a"}
      />
    </Flex>
  );
}
