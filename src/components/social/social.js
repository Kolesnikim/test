import React from "react";
// Chakra imports
import { Stylesheet, Flex, Link, Image } from "@chakra-ui/react";
// Custom components
import Ticktok from "assets/svg/icons/tiktok.svg";
import Facebook from "assets/svg/icons/facebook.svg";
import Instagram from "assets/svg/icons/instagram.svg";

export function Social({ person }) {
  // Chakra Color Mode
  return (
    <Flex>
      {person?.tiktok && (
        <Link href={person?.tiktok} target="_blank">
          <Image style={styles} src={Ticktok} />
        </Link>
      )}
      {person?.instagram && (
        <Link href={person?.instagram} target="_blank">
          <Image style={styles} src={Instagram} />
        </Link>
      )}
      {person?.facebook && (
        <Link href={person?.facebook} target="_blank">
          <Image style={styles} src={Facebook} />
        </Link>
      )}
    </Flex>
  );
}

const styles = {
  width: "24px",
  height: "24px",
  me: "2px",
};
